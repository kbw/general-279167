#ifdef _WIN32
#undef UNICODE
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")
#else
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

#include "../common.hpp"

#include <cstdio>
#include <cstring>
#include <string>

int main(int argc, char *argv[]) {
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2,2), &wsaData);
#endif
	int rc;
	const std::string server = (argc > 1) ? argv[1] : "127.0.0.1";
	const std::uint16_t port = (argc > 2) ? static_cast<std::uint16_t>(std::atoi(argv[2])) : PORT;

	// Create socket
	SOCKET sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == SOCKET_ERROR)
		fatal("socket()");

	// Build server address
	struct sockaddr_in remote_addr;
	std::memset(&remote_addr, 0, sizeof(remote_addr));
	remote_addr.sin_family = AF_INET; 
	remote_addr.sin_port = htons(port); 
	inet_pton(AF_INET, server.c_str(), &remote_addr.sin_addr); 

	// Connect to server
	rc = connect(sock, (struct sockaddr*)&remote_addr, sizeof(remote_addr));
	if (rc == SOCKET_ERROR)
		fatal("connect()");
	std::printf("[Client] Connected to %s:%d\n", server.c_str(), port);

	char buffer[LENGTH];
	ssize_t nbytes;
	int len;

	// send HELLO
	len = std::snprintf(buffer, sizeof(buffer), "HELLO: %s:%d", server.c_str(), port);
	nbytes = send(sock, buffer, len, 0);
	if (nbytes != len)
		fatal("send(HELLO)");

	// read HELLO reply
	nbytes = recv(sock, buffer, LENGTH, 0);
	if (nbytes <= 0)
		fatal("recv HELLO reply");

	// send BYE
	len = std::snprintf(buffer, sizeof(buffer), "BYE: %s:%d", server.c_str(), port);
	nbytes = send(sock, buffer, len, 0);
	if (nbytes != len)
		fatal("send(BYE)");

	// read BYE reply
	nbytes = recv(sock, buffer, LENGTH, 0);
	if (nbytes <= 0)
		fatal("recv BYE reply");

	std::printf("[Client] Done\n");
#ifdef _WIN32
	closesocket(sock);
#else
	close(sock);
#endif
}
