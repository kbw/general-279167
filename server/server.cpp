#ifdef _WIN32
#undef UNICODE
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Ws2_32.lib")
#else
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "../common.hpp"

#include <chrono>
#include <cstring>
#include <sstream>
#include <string>
#include <thread>

void handle_client(SOCKET client);

int main(int argc, char* argv[]) {
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2,2), &wsaData);
#endif
	std::srand(
		std::chrono::time_point_cast<std::chrono::microseconds>( std::chrono::steady_clock::now() )
			.time_since_epoch()
			.count() & RAND_MAX); // servers can start at around the same time
	int rc;
	const std::uint16_t port = (argc > 2) ? static_cast<std::uint16_t>(std::atoi(argv[1])) : PORT;

	// Create socket
	SOCKET sock = socket(PF_INET, SOCK_STREAM, IPPROTO_IP);
	if (sock == SOCKET_ERROR)
		fatal("socket()");
	std::printf("[Server] created socket.\n");

	// Build server address
	struct sockaddr_in addr_local;
	std::memset(&addr_local, 0, sizeof(addr_local));
	addr_local.sin_family = AF_INET;
	addr_local.sin_port = htons(port);
	addr_local.sin_addr.s_addr = INADDR_ANY;

	// Bind to address
	rc = bind(sock, (struct sockaddr*)&addr_local, sizeof(addr_local));
	if (rc == SOCKET_ERROR)
		fatal("bind()");
	std::printf("[Server] Bind to all addresses, port %d\n", port);

	// Listen for client
	rc = listen(sock, BACKLOG);
	if (rc == SOCKET_ERROR)
		fatal("listen()");
	std::printf ("[Server] Listening\n");

	while (true) {
		// accept new client
		struct sockaddr_in addr_remote;
		socklen_t size_remote = sizeof(addr_remote);
		SOCKET client = accept(sock, (struct sockaddr*)&addr_remote, &size_remote);
		if (client == SOCKET_ERROR)
			fatal("accept()");
		std::printf("[Server] Server has got connected from %s.\n", inet_ntoa(addr_remote.sin_addr));

		// Hancle client connection
		std::thread worker{handle_client, client}; /* handler closes client socket */
		worker.detach();
	}

#ifdef _WIN32
	closesocket(sock);
#else
	close(sock);
#endif
}

void do_hello(SOCKET client, const char* in);
void do_run(SOCKET client, const char* in);
void do_bye(SOCKET client, const char* in);

// Read client request -- our id from the client's point of view: host:port
// Send "id random-rating"
void handle_client(SOCKET client) {
	char in[LENGTH];
	ssize_t nbytes;

	while (true) {
		// read client request
		try {
			nbytes = recv(client, in, sizeof(in), 0);
			if (nbytes < 1)
				fatal("recv()");
			in[nbytes] = '\0';

			if (strncmp("HELLO: ", in, 7) == 0) {
				do_hello(client, in + 7);
			} else if (strncmp("RUN: ", in, 5) == 0) {
				do_run(client, in + 5);
			} else if (strncmp("BYE: ", in, 5) == 0) {
				do_bye(client, in + 5);
			}
		}
		catch (const std::exception& e) {
			nbytes = send(client, e.what(), strlen(e.what()), 0);
			break;
		}
	}

#ifdef _WIN32
	closesocket(client);
#else
	close(client);
#endif
}

void do_hello(SOCKET client, const char* in) {
	// reqeust has: "host:port"
	// reply has:   "host:port weight"
	std::ostringstream os;
	os << "HELLO: " << in << ':' << (std::rand() % 10);
	std::string out = os.str();

	ssize_t nbytes = send(client, out.c_str(), out.size(), 0);
	if (nbytes != static_cast<int>(out.size()))
		fatal("send()");
}

void do_run(SOCKET client, const char* /*in*/) {
	// reqeust has: "work to do"
	// reply has:   "result"
	sleep(std::rand() % 3);
	std::string out{"42"};

	ssize_t nbytes = send(client, out.c_str(), out.size(), 0);
	if (nbytes != static_cast<int>(out.size()))
		fatal("send()");
}

void do_bye(SOCKET /*client*/, const char* /*in*/) {
	// send message, close connection
	throw std::runtime_error{"BYE:\n"};
}
