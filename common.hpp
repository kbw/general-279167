#pragma once

#include <cstdint>
#include <cstdio>
#include <cstdlib>

constexpr std::uint16_t PORT = 30000;
constexpr int BACKLOG = 5;
constexpr std::size_t LENGTH = 512;

#ifdef _WIN32
	typedef int ssize_t;
	inline void sleep(int seconds) {
		Sleep(seconds * 1000);
	}
#else
	typedef int SOCKET;
	constexpr int SOCKET_ERROR = -1;
#endif

inline void fatal(const char *msg) {
	std::perror(msg);
	std::exit(1);
}
