#/bin/sh

CMD="${CC-clang++} -std=c++17 -pedantic -pthread -Wall -Wextra"

for app in server client; do
  (cd ${app}; rm ${app} ${app}.o ${app}.core 2>/dev/null; ${CMD} -o ${app} ${app}.cpp)
done
